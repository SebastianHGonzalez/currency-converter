import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

import CurrencyInput from './CurrencyInput';
import Styles from '../../Styles';


class CurrencyConverter extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currencies: [],
            values: props.defaultCurrencies || [
                { currency: 'EUR', amount: 1 },
                { currency: 'USD', amount: 1 },
            ]
        };

        this.reverseCurrencies = this.reverseCurrencies.bind(this);
        this.makeHandleAmountChange = this.makeHandleAmountChange.bind(this);
        this.makeHandleCurrencyChange = this.makeHandleCurrencyChange.bind(this);
    }

    componentDidMount() {
        this.props.currencyConverterService.currencies()
            .then(currencies => this.setState({ currencies }));

        this.convert(this.state.values[0], this.state.values);
    }

    convert({ currency, amount }, values) {
        const calculator = this.props.currencyConverterService.convert(currency, amount)
        const newValues = values.map(
            (oldValue) => calculator.to(oldValue.currency).then(newAmount => ({
                currency: oldValue.currency,
                amount: newAmount,
            }))
        )
        return Promise.all(newValues).then(values => this.setState({values}));
    }

    makeHandleAmountChange(index) {
        return (amount) => {
            const values = [...this.state.values];
            const newValue = { ...values[index], amount };
            values[index] = newValue;
            this.convert(newValue, values);
        }
    }

    makeHandleCurrencyChange(index) {
        return (currency) => {
            const values = [...this.state.values];
            const newValue = { ...values[index], currency };
            values[index] = newValue;
            this.convert(newValue, values);
        }
    }

    reverseCurrencies() {
        const values = this.state.values.map(({ currency }) => currency)
            .reverse()
            .map((currency, index) => ({
                amount: this.state.values[index].amount,
                currency,
            }));
        this.convert(values[0], values);
    }

    render() {
        const { classes } = this.props;
        return (
            <Card className={classes.card}>
                <CardContent>
                    {
                        this.state.values.map(({ currency, amount }, index) =>
                            <CurrencyInput key={index}
                                currency={currency}
                                amount={amount}
                                currencies={this.state.currencies}
                                onAmountChange={this.makeHandleAmountChange(index)}
                                onCurrencyChange={this.makeHandleCurrencyChange(index)}
                            />
                        )
                    }
                </CardContent>
                <CardActions>
                    <Button size="small" variant="outlined" color="primary" onClick={this.reverseCurrencies}>
                        Reverse currencies
                    </Button>
                </CardActions>
            </Card>
        );
    }
}

CurrencyConverter.propTypes = {
    currencyConverterService: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    defaultCurrencies: PropTypes.array,
};

export default withStyles(Styles)(CurrencyConverter);
