import * as React from 'react';
import PropTypes from 'prop-types';

import {
    TextField,
    withStyles,
    MenuItem,
    Grid
} from '@material-ui/core';

import Styles from '../../Styles';


function CurrencyInput({ classes, currency, currencies, amount, onAmountChange, onCurrencyChange }) {

    const handleAmountChange = ({ target }) => onAmountChange(parseFloat(target.value));
    const handleCurrencyChange = ({ target }) => onCurrencyChange(target.value);

    return (
        <Grid container className={classes.container}>
            <Grid item xs={3}>
                <TextField
                    select
                    onChange={handleCurrencyChange}
                    className={classes.textField}
                    value={currency}
                    SelectProps={
                        {
                            MenuProps: {
                                className: classes.menu,
                            },
                        }
                    }
                    margin="normal"
                    variant="outlined" >
                    {
                        currencies.map(currency => (
                            <MenuItem key={currency} value={currency}>
                                {currency}
                            </MenuItem>
                        ))
                    }
                </TextField>
            </Grid>
            <Grid item xs={9}>
                <TextField
                    placeholder="Amount"
                    type="number"
                    onChange={handleAmountChange}
                    className={
                        classes.textField
                    }
                    value={amount || ''}
                    margin="normal"
                    variant="outlined"
                />
            </Grid>
        </Grid>
    );
}

CurrencyInput.propTypes = {
    classes: PropTypes.object.isRequired,
    currency: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    currencies: PropTypes.array.isRequired,
    onAmountChange: PropTypes.func.isRequired,
    onCurrencyChange: PropTypes.func.isRequired,
};

export default withStyles(Styles)(CurrencyInput);
