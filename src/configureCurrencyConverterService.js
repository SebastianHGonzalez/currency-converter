import Axios from 'axios';
import * as cachios from 'cachios';

import CurrencyConverterService from './Services/CurrencyConverterService';
import config from './config';


export default function configureCurrencyConverterService() {
    const axiosInstance = cachios.create(
        Axios.create(config.currencyConverter), {
            stdTTL: config.stdTTL
        }
    );

    return new CurrencyConverterService(axiosInstance);
}
