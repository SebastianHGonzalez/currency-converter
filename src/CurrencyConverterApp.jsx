import * as React from 'react';
import PropTypes from 'prop-types';

import { Grid } from '@material-ui/core';

import CurrencyConverter from './Components/CurrencyConverter';


function CurrencyConverterApp({ defaultCurrencies, currencyConverterService }) {
    return (
        <Grid container alignItems="center">
            <Grid item
                xs={12}
                sm={8}
                md={5}
                lg={4}>
                <CurrencyConverter
                    defaultCurrencies={defaultCurrencies}
                    currencyConverterService={currencyConverterService}
                />
            </Grid>
        </Grid>
    );
}

CurrencyConverterApp.propTypes = {
    currencyConverterService: PropTypes.object.isRequired,
    defaultCurrencies: PropTypes.array,
};

export default CurrencyConverterApp;
