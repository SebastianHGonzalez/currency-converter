export default {
    currencyConverter: {
        baseURL: process.env.CURRENCY_CONVERTER_BASE_URL,
        params: {
            access_key: process.env.CURRENCY_CONVERTER_ACCESS_KEY,
        }
    },
    stdTTL: process.env.STD_TTL,
};
