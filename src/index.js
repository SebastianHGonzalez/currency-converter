import React from 'react';
import ReactDOM from 'react-dom';

import * as serviceWorker from './serviceWorker';

import CurrencyConverterApp from './CurrencyConverterApp';
import configureCurrencyConverterService from './configureCurrencyConverterService';
import config from './config';


ReactDOM.render(
    <CurrencyConverterApp
        defaultCurrencies={config.defaultCurrencies}
        currencyConverterService={configureCurrencyConverterService()}
    />,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
