import CurrencyConverterCalculator from './CurrencyConverterCalculator';


class CurrencyConverterService {

    constructor(http) {
        this.http = http;
    }

    convert(currency, amount) {
        return new CurrencyConverterCalculator(currency, amount, this);
    }

    rates() {
        return this.http.get(this.ratesEndpoint)
            .then(axiosResponse => axiosResponse.data)
            .then(({ rates }) => rates)
    }

    currencies() {
        return this.rates().then(Object.keys)
    }

    get ratesEndpoint() {
        return '/api/latest';
    }
}

export default CurrencyConverterService;
