export default class CurrencyConverterCalculator {

    constructor(currency, amount, currencyConverterService) {
        this.currency = currency;
        this.amount = amount;
        this.currencyConverterService = currencyConverterService;
    }

    to(currency) {
        return this.currencyConverterService.rates().then(rates =>
            rates[currency] && (
                Math.round(((this.amount * rates[currency]) / rates[this.currency]) * 10000) 
                    / 10000
                )
        );
    }
}
